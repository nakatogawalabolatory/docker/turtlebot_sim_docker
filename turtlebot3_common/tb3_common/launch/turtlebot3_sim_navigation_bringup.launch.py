import os
import traceback
from launch import LaunchDescription # Launcher 
from launch.actions import DeclareLaunchArgument, IncludeLaunchDescription, TimerAction, ExecuteProcess
from launch_ros.actions import Node
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch.substitutions import LaunchConfiguration, FindExecutable
from ament_index_python.packages import get_package_share_directory
from launch.conditions import UnlessCondition 

def generate_launch_description():
    try:
        ld = LaunchDescription()
        # get path
        tb3_common_path = get_package_share_directory('tb3_common')
        tb3_common_launch_path = os.path.join(tb3_common_path, 'launch')
        tb3_navigation_path = get_package_share_directory('tb3_navigation')
        tb3_navigation_launch_path = os.path.join(tb3_navigation_path, 'launch')
        # launch tb3_minimum
        tb3_bringup_launch_description = IncludeLaunchDescription(
            PythonLaunchDescriptionSource(
                os.path.join(tb3_common_launch_path, 'turtlebot3_sim_minimum_bringup.launch.py'),
            ),
        )

        # launch navigation
        tb3_navigation_launch_description = IncludeLaunchDescription(
            PythonLaunchDescriptionSource(
                os.path.join(tb3_navigation_launch_path, 'nav2_bringup.launch.py'),
            )
        )

        #launch_delay_to_tb3_navigation_launch_description = TimerAction(period=10.0, actions=[tb3_navigation_launch_description])
        tb3_navigation_launch_command = ExecuteProcess(
            cmd=[[
                ' source ',
                '/colcon_ws/install/setup.bash;',
                #'sleep 5;',
                'ros2',
                ' launch ',
                'tb3_navigation',
                ' nav2_bringup.launch.py'
            ]],
            shell=True
        )

        load_locations_node = Node(
            package = 'tb3_navigation',
            executable = 'location_loader.py'
        )

        #ld.add_action(declare_cam_teleop)
        ld.add_action(tb3_bringup_launch_description)
        #ld.add_action(launch_delay_to_tb3_navigation_launch_description)
        ld.add_action(tb3_navigation_launch_command)
        ld.add_action(load_locations_node)
        
        return ld
    except Exception as e:
        traceback.print_exc()
        raise e
