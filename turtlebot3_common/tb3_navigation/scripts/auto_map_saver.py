#!/usr/bin/env python3
from rclpy.node import Node
import rclpy

import traceback
import subprocess
import time
import os

def main(path='/colcon_ws/src/turtlebot3_common/tb3_navigation/map/',
        file_name = os.environ['WORLD'] + '_map'):
    rclpy.init()
    node = Node('auto_map_saver')

    cmd = 'ros2 run nav2_map_server map_saver_cli -f %s%s'%(path,file_name)
    while rclpy.ok():
        try:
            try:
                subprocess.run(cmd,
                                shell=True,
                                check=True,
                                stdout=subprocess.PIPE,
                                stderr=subprocess.PIPE)
                node.get_logger().info('saved %s'%path + file_name)
            except subprocess.CalledProcessError as e:
                node.get_logger().error('map is not exists !')
            time.sleep(5)
            
        except KeyboardInterrupt:
            node.get_logger().info('Done ...')
            break

if __name__== '__main__':
    main()
