#!/usr/bin/env python3
from nav_msgs.msg import OccupancyGrid
from geometry_msgs.msg import PoseStamped, TransformStamped, PointStamped
from rclpy.node import Node
import rclpy
import tf2_ros
import tf2_geometry_msgs

class ApproachObstacle:
    def __init__(self, node):
        self.node = node
        self.map_sub = self.node.create_subscription(OccupancyGrid, 'map', self.map_callback, 10)
        self.tf_buffer = tf2_ros.Buffer()
        self.tf_listener = tf2_ros.TransformListener(self.tf_buffer, self.node)
        #self.pose_sub = self.node.create_subscription(PoseWithCovarianceStamped, '/amcl_pose', self.pose_callback, 10)
        self.map = None
        self.robot_pose = None

    def map_callback(self, msg):
        self.map = msg
        try:
            transform = self.tf_buffer.lookup_transform('map', 'base_link', rclpy.time.Time(), timeout=rclpy.time.Duration(seconds=1.0))
            self.robot_pose = transform
            print('processing...')
            print(self.robot_pose)
            print('processing...')
            # Convert robot pose to grid coordinates
            resolution = self.map.info.resolution
            origin_x = self.map.info.origin.position.x
            origin_y = self.map.info.origin.position.y
            robot_x = self.robot_pose.pose.position.x
            robot_y = self.robot_pose.pose.position.y
            robot_theta = 2 * np.arcsin(self.robot_pose.pose.orientation.z)

            robot_grid_x = int((robot_x - origin_x) / resolution)
            robot_grid_y = int((robot_y - origin_y) / resolution)

            max_range = 100  # Max range to check for obstacles (in cells)
            for i in range(1, max_range + 1):
                check_x = robot_grid_x + int(i * np.cos(robot_theta))
                check_y = robot_grid_y + int(i * np.sin(robot_theta))

                if check_x < 0 or check_x >= self.map.info.width or \
                   check_y < 0 or check_y >= self.map.info.height:
                    continue

                if self.map.data[check_y * self.map.info.width + check_x] > 0:
                    # Obstacle found
                    print((origin_x + check_x * resolution, origin_y + check_y * resolution))
        except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException):
            self.robot_pose = None

    def pose_callback(self, msg):
        self.robot_pose = msg

def main():
    rclpy.init()
    node = Node('approach_obstacle')
    ao = ApproachObstacle(node)
    rclpy.spin(node)
    node.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()
