#!/usr/bin/env python3
# ROS1 でいう rospy をインポートする
from rclpy.node import Node # ノード宣言時に使用する
import rclpy # ROS2 版 rospy
# TB#Navigation をインポートする
from tb3_navigation.tb3_navigator import TB3Navigation

# メイン関数
def main():
    # rclpy を初期化してノードを宣言
    rclpy.init()
    node = Node('sample_navigation')

    # ノードをクラスにわたしてインスタンス化。（本家推奨の書き方ではないが柔軟に対応できる記述法）
    sn = TB3Navigation(node)

    # マップ基準で 4m 前に進む
    sn.go_abs(4.0)

    # 現在位置を基準に -2m 前に進む
    sn.go_rlt(-2.0)

    # マップの 0 座標へ移動する
    sn.go_abs()

# 単体で実行されたらコールされる
if __name__ == '__main__':
    main()
