# ROS1 でいう rospy をインポートする
from rclpy.node import Node # ノード宣言時に使用する
import rclpy # ROS2 版 rospy

# tf2
from tf_transformations import euler_from_quaternion, quaternion_from_euler
from tf2_ros import TransformListener, Buffer, TransformException

# Navigation2 API
from nav2_simple_commander.robot_navigator import BasicNavigator
from nav2_simple_commander.robot_navigator import TaskResult

# メッセージライブラリ
from geometry_msgs.msg import PoseStamped # 位置を定義するメッセージ
from geometry_msgs.msg import PoseWithCovarianceStamped # 位置を変更するメッセージ
from geometry_msgs.msg import Twist

# 標準ライブラリ
import traceback # エラーをトレースバックする
import threading
import time

from config.location_loader import location_loader

# メインクラス
class TB3Navigation:
    # インスタンス化されたときに実行されるメソッド
    def __init__(self, node):
        self.node = node # ノード
        self.logger = self.node.get_logger() # logging 用
        self.navigator = BasicNavigator()

        # TF 関連
        self.tf_buffer = Buffer()
        self.tf_listener = TransformListener(self.tf_buffer, node)

        # Navigator が起動するのを待機
        self.navigator.waitUntilNav2Active()
        self.logger.info('OK')

    def get_current_pose(self, xyy=False):
        while rclpy.ok():
            rclpy.spin_once(self.node)
            try:
                transform = self.tf_buffer.lookup_transform('map', 'base_footprint', rclpy.time.Time())
                if xyy:
                    p = []
                    p.append(transform.transform.translation.x)
                    p.append(transform.transform.translation.y)

                    e = euler_from_quaternion((transform.transform.rotation.x,
                                                transform.transform.rotation.y,
                                                transform.transform.rotation.z,
                                                transform.transform.rotation.w))
                    p.append(e[2])

                    transform = p
                    
                return transform
            except Exception as e:
                #print(e)
                pass

    # 絶対座標で任意の座標へ向かうメソッド
    def go_abs(self, x=0, y=0, yaw=0):
        #quaternion に変換
        q = quaternion_from_euler(0.0,0.0,yaw)
        # 目標座標を設定
        p = PoseStamped()
        p.header.frame_id = 'map'
        p.header.stamp = self.navigator.get_clock().now().to_msg()
        p.pose.position.x = float(x)
        p.pose.position.y = float(y)
        p.pose.orientation.x = q[0]
        p.pose.orientation.y = q[1]
        p.pose.orientation.z = q[2]
        p.pose.orientation.w = q[3]

        self.navigator.goToPose(p)
        while not self.navigator.isTaskComplete():
            feedback = self.navigator.getFeedback()
            #print(feedback)
        result = self.navigator.getResult()
        if result == TaskResult.SUCCEEDED:
            self.logger.info('Goal succeeded!')
        elif result == TaskResult.CANCELED:
            self.logger.warn('Goal was canceled!')
        elif result == TaskResult.FAILED:
            self.logger.error('Goal failed!')
        else:
            self.logger.error('Goal has an invalid return status!')

    # 相対座標で任意の座標へ向かうメソッド
    def go_rlt(self, x=0, y=0, yaw=0):
        p = self.get_current_pose()

        px = p.transform.translation.x
        py = p.transform.translation.y

        e = euler_from_quaternion((p.transform.rotation.x,
                                    p.transform.rotation.y,
                                    p.transform.rotation.z,
                                    p.transform.rotation.w))
        pyaw = e[2]

        self.go_abs(px + x, py + y, pyaw + yaw)

    # 初期位置を更新するメソッド
    def pose_estimate(self, x=0.0, y=0.0, yaw=0.0, adjust=False):
        pub = self.node.create_publisher(PoseWithCovarianceStamped, 'initialpose', 10)

        p = PoseWithCovarianceStamped()
        #quaternion に変換
        q = quaternion_from_euler(0.0,0.0,yaw)
        # 目標座標を設定
        p.header.frame_id = 'map'
        p.header.stamp = self.navigator.get_clock().now().to_msg()
        p.pose.pose.position.x = float(x)
        p.pose.pose.position.y = float(y)
        p.pose.pose.orientation.x = q[0]
        p.pose.pose.orientation.y = q[1]
        p.pose.pose.orientation.z = q[2]
        p.pose.pose.orientation.w = q[3]

        pub.publish(p)

        if adjust:
            self.go_rlt(yaw=1.0)
            self.go_rlt(yaw=-2.0)
            self.go_rlt(yaw=1.0)
            self.go_abs(x, y, yaw)

    # ウェイポイントを作成するメソッド
    def create_waypoint(self, interval_distance=0.1, interval_angle=0.7):
        init_time = time.time()
        wp = []
        n = 0
        while True:
            try:
                # ウェイポイントに追加
                p = self.get_current_pose(xyy=True)
                if len(wp) == 0:
                    print('Add start waypoint!')
                    wp.append(p)
                    n += 1
                else:
                    if abs(wp[n-1][0] - p[0]) >= interval_distance or abs(wp[n-1][1] - p[1]) >= interval_distance or abs(wp[n-1][2] - p[2]) >= interval_angle:
                        print('Add waypoint!')
                        wp.append(p)
                        n += 1

            except KeyboardInterrupt:
                self.logger.info('waypoint creator break')
                break
        return wp

    def go_waypoint(self, waypoint):
        for w in waypoint:
            self.go_abs(w[0], w[1], w[2])

    # get locations
    def go_location(self, location_name=None):
        location = location_loader()
        if location_name is None:
            self.node.get_logger().warn('please declare the location_name. location_name is here')
            location = location_loader()
            for k in location.keys():
                self.node.get_logger().warn(k)
            return False
        for k, v in location.items():
            if k == location_name:
                self.go_abs(v[0], v[1], v[2])
        
# メイン関数
def main():
    # rclpy を初期化してノードを宣言
    rclpy.init()
    node = Node('sample_navigation')

    # ノードをクラスにわたしてインスタンス化。（本家推奨の書き方ではないが柔軟に対応できる記述法）
    sn = TB3Navigation(node)
    '''
    # マップ基準で 1m 前に進む
    sn.go_abs(1.0)

    # 現在位置を基準に 2m 前に進む
    sn.go_rlt(2.0)

    # マップの 0 座標へ移動する
    sn.go_abs()
    '''
    #sn.go_abs()
    #print(sn.get_current_pose(xyy=True))
    sn.go_location('room0_1')
    sn.go_location('central')

# 単体で実行されたらコールされる
if __name__ == '__main__':
    main()
