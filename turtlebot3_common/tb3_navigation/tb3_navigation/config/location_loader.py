#!/usr/bin/env python3
from rclpy.node import Node
from visualization_msgs.msg import Marker, MarkerArray
from geometry_msgs.msg import Quaternion, Vector3, Point
from tf_transformations import quaternion_from_euler
import rclpy
import xml.etree.ElementTree as ET
import time
import os
import sys

DEFAULT_ROOM_NAME = "%s_map"%os.environ['WORLD']

def location_loader(location_file_path='/colcon_ws/src/turtlebot3_common/tb3_navigation/tb3_navigation/config',
                    location_file_name='locations', room_name=DEFAULT_ROOM_NAME):
    xml_path = location_file_path + '/' + location_file_name
    tree = ET.parse('%s.xml'%xml_path)
    root = tree.getroot()
    rooms = root.findall("./room")
    locations_dict = {}

    for room in rooms:
        if room.attrib["name"] == room_name:
            id = 0
            for location in room.findall("./location"):
                location_name = location.attrib["name"]
                global_position = [float(coord) for coord in location.attrib["global_position"].split()]
                locations_dict[location_name] = global_position
            return locations_dict
        else:
            print(f'not found locations in {room_name} or {room_name} is not exit')
            return None

def location_viewer(location_file_path='/colcon_ws/src/turtlebot3_common/tb3_navigation/tb3_navigation/config',
                    location_file_name='locations', room_name=DEFAULT_ROOM_NAME, wait=10):
    rclpy.init()
    node = Node('locations_viewer')
    location = location_loader(location_file_path, location_file_name, room_name)
    if location is None:
        node.get_logger().error(f'not found locations in {room_name} or {location_file_name} is not exit')
        sys.exit()

    time.sleep(wait)
    node.get_logger().info(f'load {room_name}')
    marker_pub = node.create_publisher(MarkerArray, 'locations', 10)
    markers = MarkerArray()

    id = 0
    for location_name, global_position in location.items():
        #print(f"Location Name: {location_name}, Global Position: {global_position}")

        marker = Marker()
        marker.header.frame_id = 'map'
        marker.header.stamp = node.get_clock().now().to_msg()
        marker.ns = location_name
        marker.id = id
        marker.type = Marker.ARROW
        marker.action = Marker.ADD
        marker.pose.position.x = global_position[0]
        marker.pose.position.y = global_position[1]
        marker.pose.position.z = 0.0
        q = quaternion_from_euler(0.,0.,global_position[2])
        marker.pose.orientation.x = q[0]
        marker.pose.orientation.y = q[1]
        marker.pose.orientation.z = q[2]
        marker.pose.orientation.w = q[3]
        marker.scale.x = 0.2
        marker.scale.y = 0.1
        marker.scale.z = 0.1
        marker.color.a = 1.0
        marker.color.r = 1.0
        marker.color.g = 0.0
        marker.color.b = 1.0
        markers.markers.append(marker)

        marker = Marker()
        marker.header.frame_id = 'map'
        marker.header.stamp = node.get_clock().now().to_msg()
        marker.ns = location_name + '_label'
        marker.id = id + 1
        marker.type = Marker.TEXT_VIEW_FACING
        marker.action = Marker.ADD
        marker.pose.position.x = global_position[0]
        marker.pose.position.y = global_position[1]
        marker.pose.position.z = 0.5
        #marker.pose.orientation = Quaternion(x=0.,y= 0., z=0., w=1.)  # テキストは常に正面を向くようにする
        marker.scale = Vector3(x=0.25, y=0.25, z=0.25)
        marker.color.a = 1.0
        marker.color.r = 1.0
        marker.color.g = 1.0
        marker.color.b = 1.0
        marker.text = location_name
        markers.markers.append(marker)

        id += 2
    while rclpy.ok():
        marker_pub.publish(markers)
        rclpy.spin_once(node)

if __name__ == '__main__':
    location_viewer()
