import os
import traceback
from launch import LaunchDescription, LaunchContext # Launcher 
from launch.actions import DeclareLaunchArgument, IncludeLaunchDescription, ExecuteProcess, LogInfo
from launch_ros.actions import Node
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch.substitutions import LaunchConfiguration, FindExecutable, PythonExpression
from ament_index_python.packages import get_package_share_directory 

def generate_launch_description():
    try:
        ld = LaunchDescription()
        lc = LaunchContext()
        
        rviz_config_dir = os.path.join(get_package_share_directory('tb3_navigation'),
                                       'rviz', 'tb3_navigation.rviz')

        # get nav2 prefix
        nav2_prefix = get_package_share_directory('nav2_bringup')
        default_map_path = os.path.join(get_package_share_directory('tb3_navigation'),
                                                    'map', os.environ['WORLD'] + '_map.yaml')

        # config
        map = LaunchConfiguration("map")
#ld.add_action(declare_param)
        nav_param = os.path.join(get_package_share_directory('tb3_navigation'),
                                                             'param',
                                                             os.environ['TURTLEBOT3_MODEL'] + '.yaml')

        use_sim_time = LaunchConfiguration("use_sim_time")

        # add actions
        declare_use_sim_time = DeclareLaunchArgument('use_sim_time',
                                            default_value='true',
                                            description='Use simulation (Gazebo) clock if true')

        declare_map = DeclareLaunchArgument('map',
                                            default_value=default_map_path,
                                            
                                            description='Navigation で使用したい map ファイルを選択してください')
        
        declare_param = DeclareLaunchArgument('params_file',
                                            default_value=nav_param,
                                            description='Navigation で使用したい param ファイルを選択してください')

        nav2_bringup = IncludeLaunchDescription(
                        PythonLaunchDescriptionSource([nav2_prefix, '/launch/bringup_launch.py']),
                        launch_arguments={'use_sim_time': use_sim_time,
                                          'map': map}.items()
                    )
                    
        rviz = Node(package='rviz2',
                    executable='rviz2',
                    arguments=['-d', rviz_config_dir])

        ld.add_action(LogInfo(msg=['LoadingPram: ', nav_param]))
        ld.add_action(declare_use_sim_time)
        #ld.add_action(declare_param)
        ld.add_action(declare_map)
        ld.add_action(nav2_bringup)
        ld.add_action(rviz)
        # Execute call map
        ld.add_action(
            ExecuteProcess(
                cmd=[[
                    'source ',
                    '/opt/ros/humble/setup.bash;',
                    'sleep 5;',
                    'ros2',
                    ' service call ',
                    '/map_server/load_map ',
                    'nav2_msgs/srv/LoadMap ',
                    '"{map_url: ',
                    map,
                    '}"'
                ]],
                shell=True
            )
        )

        return ld
    except Exception as e:
        traceback.print_exc()
        raise e
